package org.tio.showcase.http.dao;


import org.tio.http.common.HttpResponse;
import org.tio.showcase.http.model.SysUser;
import org.tio.showcase.http.model.vo.ResponseJson;

public interface SysUserDao {
    // 登录
    ResponseJson getByUsername(String username, String password) throws Exception;

    // 通过ID获取好友
    ResponseJson getByUserId(String id) throws Exception;
}
