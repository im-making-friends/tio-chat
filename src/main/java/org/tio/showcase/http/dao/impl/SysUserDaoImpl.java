package org.tio.showcase.http.dao.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.tio.http.common.HttpResponse;
import org.tio.http.server.util.Resps;
import org.tio.showcase.dbUtil.MongoConst;
import org.tio.showcase.dbUtil.MongodbUtil;
import org.tio.showcase.http.dao.SysUserDao;
import org.tio.showcase.http.model.Group;
import org.tio.showcase.http.model.SysUser;
import org.tio.showcase.http.model.po.SysUserInfo;
import org.tio.showcase.http.model.vo.ResponseJson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SysUserDaoImpl implements SysUserDao {

    // 登录
    @Override
    public ResponseJson getByUsername(String username, String password) throws Exception {
        SysUser u = new SysUser();
        u.setUsername(username);
        u.setPassword(password);
        List<Object> list = MongodbUtil.findByPage(MongoConst.USER,0,1,MongodbUtil.objectToMap(u));
        if (list.size() <= 0) {
            return new ResponseJson().error("请检查用户名，密码是否正确");
        }else{
            for (Object res: list) {
                u.setId(MongodbUtil.getObjectId(res));
                JSONObject jsonObject = JSON.parseObject(res+"");
                u.setUsername(jsonObject.getString("username"));
                u.setPassword(jsonObject.getString("password"));
                u.setPicUrl(jsonObject.getString("picUrl"));
                u.setPhone(jsonObject.getString("phone"));
                u.setNick(jsonObject.getString("nick"));
                u.setCreationTime(jsonObject.getString("creationTime"));
            }
            if(u.getUsername().equals(username)){   // 由于 mongodb 多条件查询类似mysql 模糊查询，所以在进行校验
                return new ResponseJson().success().setMsg(u.getId());
            }else{
                return new ResponseJson().error("请检查用户名，密码是否正确");
            }
        }
    }

    // 根据用户ID获取好友
    @Override
    public ResponseJson getByUserId(String id) throws Exception {

        SysUserInfo userInfo = new SysUserInfo();
        List<SysUser> userList = new ArrayList<>();;
        List<Object> list = MongodbUtil.findDocumentById(MongoConst.USER,id);
        // TODO 待完善，这里先所有所有用户
        List<Object> l = MongodbUtil.findByPage(MongoConst.USER,0,10,MongodbUtil.objectToMap(userInfo));
        for (Object res: l) {
            SysUser u = new SysUser();
            JSONObject jsonObject = JSON.parseObject(res+"");
            u.setId(MongodbUtil.getObjectId(res));
            if(u.getId().equals(id)){
                userInfo.setId(id);
                userInfo.setUsername(jsonObject.getString("username"));
                userInfo.setPassword(jsonObject.getString("password"));
                userInfo.setPicUrl(jsonObject.getString("picUrl"));
                userInfo.setPhone(jsonObject.getString("phone"));
                userInfo.setNick(jsonObject.getString("nick"));
                userInfo.setCreationTime(jsonObject.getString("creationTime"));
            }else{
                u.setUsername(jsonObject.getString("username"));
                u.setPassword(jsonObject.getString("password"));
                u.setPicUrl(jsonObject.getString("picUrl"));
                u.setPhone(jsonObject.getString("phone"));
                u.setNick(jsonObject.getString("nick"));
                u.setCreationTime(jsonObject.getString("creationTime"));
                userList.add(u);
            }
        }
        userInfo.setFriendList(userList);

        //////////////// 获取群组信息 //////////////////
        SysUser u = new SysUser();
        u.setId(id);
        Map map = new HashMap();
        map.put("userId",userInfo.getId());
        List<Object> groupid = MongodbUtil.findByPage(MongoConst.GROUP_USER,0,10,map);
        List<String> group = new ArrayList<>();
        for (Object s:groupid) {
            JSONObject jsonObject = JSON.parseObject(s+"");
            System.out.println(jsonObject);
            group.add(jsonObject.getString("groupId"));
        }
        List<Group> groupList = new ArrayList<>();
        List<Object> group_info = MongodbUtil.findDocumentById(MongoConst.GROUP,group);
        for (Object res:group_info) {
            JSONObject jsonObject = JSON.parseObject(res+"");
            Group g = new Group();
            g.setGroupName(jsonObject.getString("groupName"));
            g.setUsername(jsonObject.getString("username"));
            g.setGroupAddress(jsonObject.getString("groupAddress"));
            g.setGroupAvatarUrl(jsonObject.getString("groupAvatarUrl"));
            g.setId(MongodbUtil.getObjectId(res));
            groupList.add(g);
        }
        userInfo.setGroupList(groupList);
        return new ResponseJson().success()
                .setData("userInfo", userInfo);
    }
}
