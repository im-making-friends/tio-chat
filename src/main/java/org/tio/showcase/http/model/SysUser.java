package org.tio.showcase.http.model;

import lombok.Data;

import java.util.Objects;

/**
 * 系统用户
 */
@Data
public class SysUser {
    private String username;    // 用户名
    private String password;    // 密码
    private String nick;        // 备注
    private String sex;         // 性别
    private String creationTime;// 添加时间
    private String picUrl;      // 头像地址
    private String phone;       // 手机
    private String id;          // mongodb 自动生成的ID
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SysUser sysUser = (SysUser) o;
        return Objects.equals(username, sysUser.username) &&
                Objects.equals(password, sysUser.password) &&
                Objects.equals(nick, sysUser.nick) &&
                Objects.equals(sex, sysUser.sex) &&
                Objects.equals(creationTime, sysUser.creationTime) &&
                Objects.equals(picUrl, sysUser.picUrl) &&
                Objects.equals(phone, sysUser.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password, nick, sex, creationTime, picUrl, phone);
    }
}
