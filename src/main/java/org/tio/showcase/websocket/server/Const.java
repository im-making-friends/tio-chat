/**
 * 
 */
package org.tio.showcase.websocket.server;

/**
 * @author tanyaowu
 *
 */
public class Const {
	/**
	 * 用于群聊的group id
	 */
	public static final String GROUP_ID = "showcase-websocket";
	// 单聊
	public static final String SINGLE_CHAT_ID = "1";
	// 群聊
	public static final String GROUP_CHAT_ID = "2";

}
