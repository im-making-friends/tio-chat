package org.tio.showcase.dbUtil;

/**
 * mongo 表名
 */
public class MongoConst {
    public static final String USER = "user";
    public static final String GROUP = "group";
    public static final String GROUP_USER = "groupUser";
}
