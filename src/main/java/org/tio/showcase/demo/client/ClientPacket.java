package org.tio.showcase.demo.client;

import org.tio.core.intf.Packet;

/**
 * @author tsinghua
 * @date 2018/6/20
 */
public class ClientPacket extends Packet {

    public static final Integer PACKET_HEADER_LENGTH = 4;

    private byte[] body;

    public byte[] getBody() {
        return body;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }
}