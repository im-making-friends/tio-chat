package org.tio.showcase.demo.server;

import org.tio.core.ChannelContext;
import org.tio.core.Tio;
import org.tio.core.TioConfig;
import org.tio.core.intf.GroupListener;

public class ImGroupListener implements GroupListener {

    public ImGroupListener(){

    }

    @Override
    public void onAfterBind(ChannelContext channelContext, String group) throws Exception {

    }

    @Override
    public void onAfterUnbind(ChannelContext channelContext, String group) throws Exception {
        TioConfig tioConfig = channelContext.tioConfig;
        ServerPacket serverPacket = new ServerPacket();
        Tio.sendToGroup(tioConfig,group,serverPacket);
    }
}
