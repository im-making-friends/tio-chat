var LayimHandler = function () {

  this.onopen = function (event, ws) {
    // ws.send('hello 连上了哦')
    //document.getElementById('contentId').innerHTML += 'hello 连上了哦<br>';
  }

  /**
   * 收到服务器发来的消息
   * @param {*} event
   * @param {*} ws
   */
  this.onmessage = function (event, ws) {
    //var json = JSON.parse(event.data);
    var data = event.data;
    var json=eval('(' + data + ')');
    var type = json.type;
    var obj = {
        username: json.username
        ,avatar: json.avatar
        ,id: json.id
        ,type: json.ustype
        ,content: json.data
    }
      if(ismobile()){
          layui.mobile.layim.getMessage(obj);
      }else {
          layui.layim.setChatStatus('<span style="color:#FF5722;">在线</span>');
          layui.layim.getMessage(obj);
      }




     /* switch (type + "") {
          //单聊
          case "1":

              // singleReceive(json);
              break;
          //群聊
          case "2":
              groupReceive(json);
              break;
          //单聊文件
          case "3":
              singleReceive(json);
              break;
          //群聊文件
          case "4":
              ws.groupReceive(json);
              break;
          //文件上传成功通知
          case "5":
              ws.fileMsgSingleRecieve(json);
              break;
          //文件到达成功
          case "6":
              ws.fileMsgGroupRecieve(json);
              break;
          default:
              console.log("不正确的类型！");
      }*/

  }

  this.onclose = function (e, ws) {
    // error(e, ws)
  }

  this.onerror = function (e, ws) {
    // error(e, ws)
  }

  /**
   * 发送心跳，本框架会自动定时调用该方法，请在该方法中发送心跳
   * @param {*} ws
   */
  this.ping = function (ws) {
    // log("发心跳了")
    ws.send('心跳内容')
  }
}

function ismobile() {
    var mobileArry = ["iPhone", "iPad", "Android", "Windows Phone", "BB10; Touch", "BB10; Touch", "PlayBook", "Nokia"];
    var ua = navigator.userAgent;
    var res=mobileArry.filter(function(arr) {
        return ua.indexOf(arr) > 0;
    });
    return res.length > 0;
}